// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDnMNMheV4l-tKobjEVyaYvQ7iLEKjgNeY",
    authDomain: "firstdb-5bf85.firebaseapp.com",
    databaseURL: "https://firstdb-5bf85.firebaseio.com",
    projectId: "firstdb-5bf85",
    storageBucket: "firstdb-5bf85.appspot.com",
    messagingSenderId: "811409244127"
  }
};
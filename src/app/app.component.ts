import { Component, OnInit} from '@angular/core';
import 'rxjs/add/operator/first';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import {FormGroup, FormControl,FormBuilder,Validators} from '@angular/forms';

import { Gramshaba } from './gramshaba';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name: any;
  message:String;
  flag:boolean=false;
  disctricCount: Observable<{}[]>;
  itemsCollection: AngularFirestoreCollection<{}>;
  statsCollection: AngularFirestoreCollection<{}>;
  public Counts:any = [
   
  ];
 
  
    gramshabaForm: FormGroup;
    gramshaba: Gramshaba= new Gramshaba();
    disticts: string[] = ['ARIYALUR','CHENNAI','COIMBATORE','CUDDALORE','DHARMAPURI','DINDIGUL','ERODE','KANCHEPURAM','KANNIYAKUMARI','KARUR','KRISHNAGIRI','MADURAI','NAGAPATTINAM','NAMAKKAL','NILGIRIS','PERAMBALUR','PUDUKKOTTAI','RAMANATHAPURAM','SALEM','SIVAGANGAI','THANJAVUR','THENI','THIRUNELVELI','THIRUVALLUR','THIRUVARUR','THOOTHUKUDI','TIRUCHIRAPPALLI','TIRUPPUR','TIRUVANNAMALAI','VELLORE','VILLUPURAM','VIRUDHUNAGAR'];
    distictsColor: string[] = ['#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA','#D0E6CA'];

    default: string = 'CHENNAI';
    items: Observable<any[]>;
    constructor(public fb:FormBuilder,public db: AngularFirestore){

    this.itemsCollection = db.collection('registration');
    this.itemsCollection.valueChanges().subscribe((itemsResponse) => {
      
      
    });
    
  }
      

 ngOnInit(): void{
      this.gramshabaForm = this.fb.group({
        name:['',[Validators.required]],
        phone:['',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
        distict:[this.default],
        village:['',[Validators.required]],
     })

    
     this.statsCollection = this.db.collection<any>('Stats');
     this.disctricCount = this.statsCollection.valueChanges();
     this.disctricCount.subscribe((Counts)=>{
       this.Counts.splice(0,this.Counts.length)
      this.Counts.push(...Counts);
      console.log(this.Counts);
      
       

     });





  }  

 save() {
       
        this.itemsCollection.add({
          name: this.gramshabaForm.value.name,
          district: this.gramshabaForm.value.distict,
          village: this.gramshabaForm.value.village,
          mobile:this.gramshabaForm.value.phone
        
      });

      let distIndex = this.disticts.indexOf(this.gramshabaForm.value.distict);
      this.distictsColor[distIndex] = "#ff0000";

      let registeredDistrict = this.gramshabaForm.value.distict;
      let districtDoc =this.db.doc<any>('Stats/' + registeredDistrict);
      districtDoc.valueChanges().first().subscribe((districtResponse) => {
        if (districtResponse) {
          districtDoc.update({count: districtResponse.count + 1});
        } else {
          let districtCollection = this.db.collection('Stats');
          districtCollection.doc(registeredDistrict).set({
  
            district:registeredDistrict,
            count:0
  
          });
         
        }
      })

  this.message="Success!Please check the District count of "+this.gramshabaForm.value.distict;
        this.gramshabaForm.reset();
       //this.gramshabaForm.value.distict="";
         //this.gramshabaForm.value.village="";
        //this.gramshabaForm.value.phone="";
      
      
      
      
     
    }

}
